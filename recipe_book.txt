Introduction:
In the following recipe book, you will find some of the most known and delicious foods.
These foods range from starters, to main courses, to desserts. With both the ingredients
and recipes listed down below, you will find yourself easily cooking and more importantly,
eating, these great foods.

Starters:

Prawn Cocktail

Ingredients:
1. 75g light mayonaise
2. 20g ketchup
3. Dash of Worcestershire sauce
4. 200g cooked prawns
5. Sprinkle paprika
6. Lemon slices


Instructions:
1. Measure out the mayonnaise, ketchup and Worcestershire sauce and mix them together in a bowl.

2. Add the prawns and mix until completely coated, then cover and pop in the fridge until ready to serve.

3. To serve, place spoonfuls of the prawn cocktail mixture onto small lettuce leaves or use as a healthy baked potato topping.

4. Sprinkle over a little paprika and serve with fresh lemon slices on the side.


Bruschetta:
Ingredients:
1. 6 or 7 ripe tomatoes (about 680g)
2. 2 cloves garlic, minced (about 2 teaspoons)
3. 1 Tbsp extra virgin olive oil
4. 1 teaspoon balsamic vinegar
5. 6-8 fresh basil leaves
6. 3/4 teaspoon sea salt
7. 1/2 teaspoon freshly ground black pepper
8. 1 french baguette

Instructions:
1. Blanch and peel the tomatoes: Bring 2 quarts of water to a boil. As the water is heating make shallow cuts in a cross pattern at the tip ends of the tomatoes (this will make the tomatoes easier to peel).

2. Once the water is boiling, remove the pot from the heat. Put the tomatoes in the hot water and blanch for 1 minute.

3. Remove with a slotted spoon and let sit until cool enough to handle. Then gently peel off the tomato skins. Cut out the stem base with a paring knife.

4. Cut the tomatoes into halves or quarters and squeeze out most of the juices and seeds.

5. Preheat oven to 450°F (230°C) with a rack in the top slot of the oven.

6. Chop tomatoes, toss them with garlic, olive oil, vinegar, basil, salt and pepper: Finely chop the tomatoes and place them in a medium bowl. Mix in the minced garlic, 1 Tbsp extra virgin olive oil, and the balsamic vinegar.

7. Stir in the thinly sliced basil and add salt and freshly ground black pepper, adding more to taste. Note, tomatoes love salt; you may need to add more than you expect.

8. Toast the baguette slices: Use a bread knife to slice the baguette on the diagonal making half-inch thick slices. Brush one side of each slice with olive oil (a pastry brush helps here) and place olive oil-side down on a baking sheet or roasting pan.

9. The baguette slices will toast best in the top rack of your oven, so you may need to work in batches to toast them all.

10. When the oven has reached 450°F (230°C) place the slices in the oven on the top rack and toast for 5 to 6 minutes until lightly browned around the edges.

11. If you want you can toast the bread slices without coating them first in olive oil. Toast them until lightly browned on both sides. Then cut a clove of garlic in half and rub over one side of the toast. Then brush with olive oil. (See Easiest Ever Garlic Bread.)

12. Serve toasted bread with tomato mixture: Arrange the toasted bread on a platter, olive oil side facing up (the olive oil will help create a temporary barrier keeping the bread from getting soggy from the chopped tomatoes).

13. Either serve the toasts plain with a bowl of the tomato bruschetta mixture on the side for people to top their own, or use a spoon to gently top each toasted bread slice with some of the tomato mixture. If you top each slice individually, do it right before serving.


Mushroom Soup:
Ingredients:
1. 1/4 cup unsalted butter
2. 2 pounds sliced fresh mushrooms
3. 1 pinch salt
4. 1 yellow onion, diced
5. 1 1/2 tablespoons all-purpose flour
6. 6 sprigs fresh thyme
7. 2 cloves garlic, peeled
8. 4 cups chicken broth
9. 1 cup water
10. 1 cup heavy whipping cream
11. 1 pinch salt and freshly ground black pepper to taste
12. 1 teaspoon fresh thyme leaves for garnish, or to taste

Instructions:
1. Melt butter in a large soup pot over medium-high heat; cook mushrooms in butter with 1 pinch salt until the mushrooms give off their juices; reduce heat to low. Continue to cook, stirring often, until juices evaporate and the mushrooms are golden brown, about 15 minutes. Set aside a few attractive mushroom slices for garnish later, if desired. Mix onion into mushrooms and cook until onion is soft and translucent, about 5 more minutes.

2. Stir flour into mushroom mixture and cook, stirring often, for 2 minutes to remove raw flour taste. Tie thyme sprigs into a small bundle with kitchen twine and add to mushroom mixture; add garlic cloves. Pour chicken stock and water into mushroom mixture. Bring to a simmer and cook for 1 hour. Remove thyme bundle.

3. Transfer soup to a blender in small batches and puree on high speed until smooth and thick.

4. Return soup to pot and stir in cream. Season with salt and black pepper and serve in bowls, garnished with reserved mushroom slices and a few thyme leaves.

Main Course:

Fettuccine Alfredo
1. 450g Fettuccine Pasta
2. 6 Tablespoons Butter
3. 1 Garlic Clove (minced)
4. 1 and 1/2 cups Heavy Cream
5. 1/4 teaspoon Salt
6. 1 and 1/4 cup Shredded Parmesan Cheese
7. 1/4 teaspoon Pepper
8. 2 Tablespoons Italian Parsley


Instructions:
1. Start by bringing water to a boil in a large pot. Add salt to the water to season the pasta. Once it is boiling, add pasta and cook according to package instructions.

2. Saute the garlic and butter together until softened — about 1 to 2 minutes.

3. Add heavy cream and let simmer over medium-high heat for about 5 minutes to thicken. Add salt and pepper to taste. Add half of the parmesan cheese to the mixture and whisk well until smooth.

4. Save some pasta water. The pasta water is full of flavor and can be used to thin out the sauce.

5. Toss alfredo sauce with fettuccine pasta and add half of the parmesan cheese. Once it is tossed, garnish with the remaining parmesan cheese. Add a little pasta water if it needs to be thinned out.

6. Garnish with Italian parsley, if so desired.


Beef Fillet
1. 4 fillet steaks, each weighing 175g
2. 2 tbsp of black peppercorns
3. 2 tbsp of green peppercorns
4. 3 tbsp of sunflower oil
5. 75g of unsalted butter
6. 1 tbsp of double cream
7. 8 tbsp of stock

Instructions:
1. Crush the peppercorns together using a pestle and mortar or a coffee grinder. Tip the pepper into a sieve set over a bowl and shake well to remove all the powdered pepper. In this recipe you want to use the cracked pepper left in the sieve; save the powdered pepper for seasoning other dishes.

2. Press the cracked peppercorns onto both sides of each steak. Put a baking tray or large plate in the oven and turn it to a very low setting ready to keep the cooked steaks warm later.

3. Heat the oil in a thick-bottomed frying pan over a medium heat. Season Once it is hot, season the steaks with salt and pace in the pan until a golden crust is formed. Turn the steaks and cook for a further 2 minutes. If you require your meat cooked further, turn the steaks and continue cooking but resist turning them too often. Remove the steaks to the warm baking tray or plate and place in the oven.

4. Add two-thirds of the butter to the frying pan and allow it to melt and turn nut brown in colour. Add the brandy, being careful in case it ignites. Bring to the boil and deglaze the pan, stirring to scrape up any bits from the bottom. Add the cream, plus the stock and bring to the boil.

5. Place the steaks on warm serving plates. Add the remaining butter to the sauce and whisk to combine. Strain the sauce if you wish. Pour it over the steaks and serve with a side of your choice.

Pizza Norma
1. 2 eggs
2. 1 cup all-purpose flour
3. 1/2 teaspoon salt
4. 1/4 teaspoon ground black pepper
5. 1/2 teaspoon dried oregano
6. 1 large eggplant, sliced into 1/2 inch rounds
7. 1/4 cup vegetable oil
8. 1 (14 ounce) can pizza sauce
9. 1 1/2 cups shredded mozzarella cheese


Instructions:
1. Preheat an oven to 350 degrees F (175 degrees C).

2. Beat the eggs in a bowl. Mix the flour, salt, pepper, and oregano in a 1 gallon resealable plastic bag. Dip each eggplant slice in the egg, then drop the eggplant in the flour mixture one at a time, shaking the bag to coat the eggplant.

3. Heat the vegetable oil in a large, deep skillet over medium heat. Place the eggplant slices in the skillet to cook, turning occasionally, until evenly browned. Drain the eggplant slices on a paper towel-lined plate. Arrange the eggplant in one layer on a baking sheet. Spoon enough pizza sauce to cover each eggplant slice. Top each eggplant with mozzarella cheese.

4. Bake in the preheated oven until the mozzarella cheese is melted, 5 to 10 minutes.

Desserts:

Banoffe-Pie:
Ingredients:
1. For the base:
1.1. 100g butter, melted
1.2. 225g light digestive biscuits, crushed

2. For the caramel:
2.1. 75g butter
2.2. 75g dark brown soft sugar
2.3. 397g can Carnation Condensed Milk

3. For the top:
3.1. 2 medium bananas
3.2. 150ml carton whipping cream, lightly whipped
3.3. Chocolate Shavings or Cocoa powder, to dust

4. Also needed:
4.1. 20cm loose-bottomed cake tin, greased

Instructions:
1. Put the biscuit crumbs in your bowl, then tip in
the melted butter and mix it all together. Spoon this
into the base and press against the bottom and sides –
this is the base of the banoffee pie recipe. Chill it
for ten minutes.

2. Place 75g butter and sugar into a non-stick saucepan
over a low heat, stirring all the time until the sugar
has dissolved. Add the condensed milk and bring to a rapid
boil for about a minute, stirring all the time for a thick
golden caramel. Spread the caramel over the base, cool and
then chill for about 1 hour, until firm or until ready to serve.

3. Carefully lift the pie from the tin and place on a serving plate.
Slice the bananas; fold half of them into the softly whipped cream
and spoon over the base. Decorate with the remaining bananas and finish
with dusting cocoa powder on top or use grated chocolate to decorate the pie!

Tips:
1. Base too crumbly? Make sure you use a buttery spread which is minimum
70% fat or use butter. Or warm the buttery/biscuit mixture briefly in
the microwave which will help it stick together. Make sure it’s well
chilled before you go to the next step.

2. Caramel too runny? It didn’t get hot enough… The mixture has to bubble
for 1 minute until its starts to thicken but not more than that or it
could catch and go grainy.

3. Caramel is too thick or grainy? It got too hot… Always use a non-stick pan,
keep stirring so it doesn’t catch and use a timer.

Lemon Cheesecake:
Ingredients:
1. 110g digestive biscuits
2. 50g butter
3. 25g light soft brown sugar
4. 350g mascarpone cheese
5. 75g caster sugar
6. 1 lemon, zested
7. 2-3 lemons, juiced (about 90ml)

Instructions:
1. To make the base, crush 110g digestive biscuits in a food bag with a
rolling pin or in the food processor.

2. Melt 50g butter in a saucepan, take off heat and stir in 25g light soft
brown sugar and the biscuit crumbs.

3. Line the base of a 20cm loose bottomed cake tin with baking parchment.
Press the biscuit into the bottom of the tin and chill in the fridge while making the topping.

4. Beat together 350g mascarpone cheese, 75g caster sugar, the zest of 1 lemon
and juice of 2-3 lemons, until smooth and creamy.

5. Spread over the base and chill for a couple of hours.

Home Made Sorbet:
Ingredients:
1. 2 pounds fresh fruit (4 to 5 cups after prepping and slicing)
2. 1 cup granulated sugar
3. 1 cup water
4. 1 large egg in its shell
5. 1 to 4 tablespoons freshly squeezed lemon juice

Instructions:
1. Freeze the ice cream base if needed. At least 24 hours before
making the sorbet, place the ice cream base in the freezer to freeze
if your ice cream maker requires a frozen base.

2. Prepare the fruit. Wash and dry the fruit. Cut away or remove any
rinds, peels, pits, seeds, stems, or other non-edible parts of the fruit.
Slice the fruit into bite-sized pieces. You should have around 5 cups of
chopped fruit, though a little more or less is fine.

3. Prepare the simple syrup. Combine the sugar and water in a small
saucepan and bring to a simmer over medium-high heat, stirring gently
once or twice. Simmer until the sugar is completely dissolved in the water,
about 5 minutes. Remove from heat and allow to cool.

4. Prepare for blending. Combine the fruit and 1/2 cup of the cooled simple
syrup in the a blender, the bowl of a food processor, or in a mixing bowl
(if using an immersion blender). Reserve the remaining syrup.

5. Blend until the fruit is completely liquified. Blend the fruit and the syrup
until the fruit is completely liquified and no more chunks of fruit remain.

6. Strain the juice. If your fruit contains small seeds (like strawberries or raspberries)
or is very fibrous (like mangos or pineapples), strain it through a fine-mesh strainer
to remove the solids. Gently stir with a spoon as you strain, but don't force the solids
through the strainer.

7. Test the sugar levels with the egg-float test. Wash and dry a large egg.
Gently lower the egg, still in its shell, into the sorbet base. You're looking
for just a small nickel-sized (roughly 1-inch) round of shell to show above the
liquid — this indicates that you have the perfect balance of juice and sugar.
If you see less shell (dime-sized), stir in a little more sugar syrup; check with
an egg and continue adjusting as needed. If you see more shell (quarter-sized),
stir in a little water or fruit juice; check with an egg and continue adjusting as
needed. (Store leftover simple sugar in the fridge.)

8. Stir in the lemon juice. Stir in 1 tablespoon of the lemon juice. Taste the
sorbet base and add more lemon juice if it tastes too sweet and bland.

9. Chill the base. Cover the sorbet base and refrigerate until very cold, at
least 1 hour or overnight.

10. Churn the sorbet. Pour the chilled base into the ice cream machine and churn.
Continue churning until the sorbet is the consistency of a thick smoothie. This typically
takes between 10 and 15 minutes in most machines.

11. Freeze the sorbet. Transfer the sorbet to pint containers or other freezable
containers and cover. Freeze for at least 4 hours, until the sorbet has hardened.
Homemade sorbet will generally keep for about a month in the freezer before starting to
become overly icy.

12. Serve the sorbet. Let the sorbet soften for a few minutes on the counter, then scoop
into serving bowls.

Pizza-Pasta:

Hawaiian Pizza
1. 1/2 recipe homemade pizza crust
2. 1/2 cup (127g) pizza sauce (homemade or store-bought)
3. 1 and 1/2 cups (6oz or 168g) shredded mozzarella cheese
4. 1/2 cup (75g) cooked ham or Canadian bacon, sliced or chopped
5. 1/2 cup (82g) pineapple chunks (canned or fresh)
6. 3 slices bacon, cooked and crumbled


Instructions

1. Prepare pizza dough through step 5, including preheating the oven to 475°F (246°C). Cover the shaped dough lightly with plastic wrap and allow it to rest as the oven preheats.

2. To prevent the pizza toppings from making your pizza crust soggy, brush the shaped dough lightly with olive oil. Using your fingers, push dents into the surface of the dough to prevent bubbling. Top the dough evenly with pizza sauce, then add the cheese, ham, pineapple, and bacon.

3. Bake pizza for 12-15 minutes. Remove from the oven and top with fresh basil, if desired. Slice hot pizza and serve immediately.

4. Cover leftover pizza tightly and store in the refrigerator. Reheat as you prefer. Baked pizza slices can be frozen up to 3 months. See pizza crust recipe for instructions on freezing the pizza dough.


BBQ Chicken Pizza

1. 1/2 recipe homemade pizza crust
2. 1–2 teaspoons olive oil
3. 1/3 cup + 2 Tablespoons your favorite BBQ sauce
4. 1 cup chopped or shredded cooked chicken (about one 8-ounce breast)
5. 2/3 cup shredded mozzarella cheese
6. 2/3 cup shredded smoked gouda cheese
7. 1/2 small red onion, thinly sliced


Instructions

1. Prepare pizza dough through step 5, including preheating the oven to 475°F (246°C). Cover the shaped dough lightly with plastic wrap and allow it to rest as the oven preheats.

2. To prevent the pizza toppings from making your pizza crust soggy, brush the shaped dough lightly with olive oil. Using your fingers, push dents into the surface of the dough to prevent bubbling. Top the dough evenly with 1/2 cup BBQ sauce. Toss the cooked chicken with the remaining BBQ sauce, then scatter all around the pizza. Add the cheeses and red onion.

3. Bake pizza for 12-15 minutes. Remove from the oven and top with fresh cilantro, if desired. Slice hot pizza and serve immediately.

4. Cover leftover pizza tightly and store in the refrigerator. Reheat as you prefer. Baked pizza slices can be frozen up to 3 months. See pizza crust recipe for instructions on freezing the pizza dough.


Pasta Carbonara:
Ingredients:
1. 100g pancetta
2. 50g pecorino cheese
3. 50g parmesan
4. 3 large eggs
5. 350g spaghetti
6. 2 plump garlic cloves, peeled and left whole
7. 50g unsalted butter
8. sea salt
9. freshly grated black pepper

Instructions:
1. Put a large saucepan of water on to boil.

2. Finely chop the 100g pancetta, having first removed any rind. Finely grate
50g pecorino cheese and 50g parmesan and mix them together.

3. Beat the 3 large eggs in a medium bowl and season with a little freshly
grated black pepper. Set everything aside.

4. Add 1 tsp salt to the boiling water, add 350g spaghetti and when the water
comes back to the boil, cook at a constant simmer, covered, for 10 minutes or
until al dente (just cooked).

5. Squash 2 peeled plump garlic cloves with the blade of a knife, just to bruise it.

6. While the spaghetti is cooking, fry the pancetta with the garlic. Drop 50g unsalted
butter into a large frying pan or wok and, as soon as the butter has melted, tip in
the pancetta and garlic.

7. Leave to cook on a medium heat for about 5 minutes, stirring often, until the
pancetta is golden and crisp. The garlic has now imparted its flavour, so take it
out with a slotted spoon and discard.

8. Keep the heat under the pancetta on low. When the pasta is ready, lift it from the
water with a pasta fork or tongs and put it in the frying pan with the pancetta. Don’t worry
if a little water drops in the pan as well (you want this to happen) and don’t throw the
pasta water away yet.

9. Mix most of the cheese in with the eggs, keeping a small handful back for sprinkling over later.

10. Take the pan of spaghetti and pancetta off the heat. Now quickly pour in the eggs and
cheese. Using the tongs or a long fork, lift up the spaghetti so it mixes easily with the
egg mixture, which thickens but doesn’t scramble, and everything is coated.

11. Add extra pasta cooking water to keep it saucy (several tablespoons should do it).
You don’t want it wet, just moist. Season with a little salt, if needed.

12. Use a long-pronged fork to twist the pasta on to the serving plate or bowl. Serve
immediately with a little sprinkling of the remaining cheese and a grating of black pepper.
If the dish does get a little dry before serving, splash in some more hot pasta water and
the glossy sauciness will be revived.

Vegan Recipes:

Vegan Cabbage Detox Soup
1. 3 cups coarsely chopped green cabbage
2. 2 1/2 cups vegetable broth
3. 1 (14.5 ounce) can diced tomatoes
4. 3 carrots, chopped
5. 3 stalks celery, chopped
6. 1 onion, chopped
7. 2 cloves garlic
8. 2 tablespoons apple cider vinegar
9. 1 tablespoon lemon juice
10. 2 teaspoons dried sage

Instructions:
1. Combine cabbage, vegetable broth, diced tomatoes, carrots, celery, onion, garlic, apple cider vinegar, lemon juice, and sage in a multi-functional pressure cooker (such as Instant Pot(R)). Close and lock the lid. Select high pressure according to manufacturer's instructions; set timer for 15 minutes. Allow 10 to 15 minutes for pressure to build.

2. Release pressure using the natural-release method according to manufacturer's instructions, 10 to 40 minutes. Unlock and remove lid.

Vegetarian Chili

1. 1 tablespoon olive oil
2. 1/2 medium onion, chopped
3. 2 bay leaves
4. 1 teaspoon ground cumin
5. 2 tablespoons dried oregano
6. 1 tablespoon salt
7. 2 stalks celery, chopped
8. 2 green bell peppers, chopped
9. 2 jalapeno peppers, chopped
10. 3 cloves garlic, chopped
11. 2 (4 ounce) cans chopped green chile peppers, drained
12. 2 (12 ounce) packages vegetarian burger crumbles
13. 3 (28 ounce) cans whole peeled tomatoes, crushed
14. 1/4 cup chili powder
15. 1 tablespoon ground black pepper
16. 1 (15 ounce) can kidney beans, drained
17. 1 (15 ounce) can garbanzo beans, drained
18. 1 (15 ounce) can black beans
19. 1 (15 ounce) can whole kernel corn

Spinach-Ricotta Quiche

1. 1 tablespoon butter
2. 1/3 cup finely chopped red onion
3. 1 (8 ounce) package fresh spinach
4. 1/2 (14 ounce) package pastry for 9-inch double-crust pie
5. 4 large eggs
6. 3/4 cup whole-milk ricotta cheese
7. 3/4 cup heavy cream
8. 1/3 cup grated Parmigiano-Reggiano cheese
9. 1 tablespoon chopped fresh basil
10. 1/2 teaspoon salt
11. 1/4 teaspoon ground black pepper

Instructions:

1. Melt butter in a skillet over medium heat; add onion and cook for 1 minute. Add spinach and cook, stirring occasionally, until starting to wilt, about 1 minute. Cover skillet and cook for 1 minute more, allowing condensation to build inside the skillet. Remove from heat, uncover, and stir.
2. Preheat the oven to 375 degrees F (190 degrees C). Press pie pastry into a 9 1/2-inch deep-dish pie pan. Prick bottom and sides with a fork.

3. Bake crust in the preheated oven for 10 minutes. Remove from oven and set aside.

4. Combine eggs, ricotta cheese, cream, Parmigiano Reggiano cheese, basil, salt, and pepper in a blender or food processor. Blend until smooth.

5. Spread spinach and onion mixture evenly over the crust. Pour egg mixture on top.

6. Bake in the preheated oven until center of quiche is set and the top is lightly browned, about 40 minutes. Allow to stand for 10 minutes before serving.

Conclusion:

This concludes the first book published, which covers a range of authentic dishes. We hope that the listed recipes will motivate individuals to develop a passion for cooking or deducing new ideas from the ones listed.
